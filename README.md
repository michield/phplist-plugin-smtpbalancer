# phplist-plugin-smtpbalancer

Spread the load of sending between a set of SMTP servers
with weighing.

Weighting is determined by the value of 'weight' for each SMTP server. Any number can be used for weight; they need not add up to 100 in total (as a percentage). Messages will be distributed among the specified SMTP servers sequentually, sending exactly the number of messages to each defined by `weight`. E.g. Once 80 messages have been sent to `smtp1` in the example below, then the following 10 messages will be sent to `smtp2`. No percentage calculation is performed; therefore balancing will distribute messages evenly regardless of the campaigns queued or their owners. It is recommended not to use large integers for `weight` however (e.g 1000+) as this may cause delays in the event of failed SMTP connections.

Each unit of `weight` translates to a single attempt to pass an email to the SMTP server identified. Success does not indicate successful message delivery, only that the message was accepted by the designated SMTP server for further processing.

Likely causes of failed SMTP dispatch/connection attempts include:

* The SMTP server is too busy and is rejecting further incoming mail
* The SMTP server is unreachable due to networking issues
* The hostname of the SMTP server is invalid or (in case an IP address is not specified) lookup of the IP corresponding to the hostname fails

## Performance

SMTP server connections and therefore queue processing will likely perform faster if the value of `hostname` is an IP address.

## Configuration

To configure, define `$smtpRelays` as an array in the standard phpList `config.php` file.

This example array will send up to 80 messages to `smtp1`, 10 to `smtp2`, 5 to `smtp3` and 5 to `smtp4`.

``` php
    $smtpRelays = array(
        // 'smtp1' can be any name - this is used for labelling purposes only
        'smtp1' => array(
            // Can be an IP address
            // On phpList Hosted this can be a constant like MX18 (these will alreay have been defined by the time this config file is loaded)
            // On phpList Hosted this *cannot* be a constant like MXGREEN because those are defined as a semicolon separated list of hosts which will not be understood
            'host' => 'localhost',
            'port' => 25,
            // This must be set. Can be any positive integer
            'weight' => 80,
            // This must not be changed (must be zero) - it is for internal use by the plugin only
            'count' => 0,
        ),
        'smtp2' => array(
            'host' => 'localhost',
            'port' => 26,
            'weight' => 10,
            'count' => 0,
        ),
        'smtp3' => array(
            'host' => 'localhost',
            'port' => 27,
            'weight' => 5,
            'count' => 0,
        ),
        'smtp4' => array(
            'host' => 'localhost',
            'port' => 28,
            'weight' => 5,
            'count' => 0,
        ),
    );
```