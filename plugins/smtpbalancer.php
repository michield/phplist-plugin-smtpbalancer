<?php

class smtpbalancer extends phplistPlugin implements EmailSender
{
    public $name = "SMTP balancer";
    public $coderoot = 'smtpbalancer/';
    public $version = "0.1";
    public $authors = 'Michiel Dethmers';
    public $enabled = 1;
    public $description = 'Spread sending across multiple SMTP servers';
    public $documentationUrl = 'https://resources.phplist.com/plugin/smtpbalancer';
    public $commandlinePluginPages = array();
    private $relays = array();
    private $relayIndexes = array();
    private $relayWeight = array();
    private $current = '';
    private $currentCount = 0;

    public $DBstruct = array(
        'smtprelay' => array(
            'id' => array('integer not null primary key auto_increment', 'ID'),
            'host' => array('text not null', ''),
            'port' => array('varchar(100) not null', ''),
            'weight' => array('integer default 0', ''),
            'count' => array('integer default 0', ''),
        ),
    );

    function __construct()
    {
        parent::__construct();

        if (isset($GLOBALS['smtpRelays'])) {
            $this->relays = $GLOBALS['smtpRelays'];
        } else {
            $this->relays = array();
        }
        $this->relayIndexes = array_keys($this->relays);
        $this->current = 0;
    }

    public function initialise()
    {
        parent::initialise(); // creates the table
    }

    public function activate()
    {
    }

    function upgrade($previous)
    {
        parent::upgrade($previous);
        return true;
    }

    function sendReport($subject, $message)
    {
        # return true;
    }

    public function dependencyCheck()
    {
        return array(
            'phpList version 3.2.0 or later' => version_compare(VERSION, '3.2') > 0,
        );
    }

    /**
     * @param PHPlistMailer $mailer
     * @param $header
     * @param $body
     * @return bool
     * @throws phpmailerException
     */
    public function send(PHPlistMailer $mailer, $header, $body) {
        $mailer->Mailer = 'smtp';
        if (empty($GLOBALS['commandline'])) {
            if (defined('PHPMAILERPORT')) {
                $mailer->Port = PHPMAILERPORT;
            }
            $mailer->Host = PHPMAILERTESTHOST;
        } elseif (count($this->relays)) {
            $relayHost = $this->relays[$this->relayIndexes[$this->current]];

            $mailer->Host = $relayHost['host'];
            $mailer->Port = $relayHost['port'];

            $this->relays[$this->relayIndexes[$this->current]]['count']++;

            if ($this->relays[$this->relayIndexes[$this->current]]['count'] >= $this->relays[$this->relayIndexes[$this->current]]['weight']) {
 #               cl_output('SMTPbalancer '.$mailer->Host.' '.$this->relayIndexes[$this->current]['count']);
                $this->relays[$this->relayIndexes[$this->current]]['total'] += $this->relays[$this->relayIndexes[$this->current]]['count'];
                $this->current++;
            }
            if ($this->current >= count($this->relays)) {
            #    cl_output('SMTPbalancer');
                $this->current = 0;
                foreach ($this->relays as $index => $data){
                    $this->relays[$index]['count'] = 0;
                }
            }
        } elseif (defined(PHPMAILERBLASTHOST)) {
            $mailer->Host = PHPMAILERBLASTHOST;
            $mailer->Port = PHPMAILERBLASTPORT;
        } else {
            $mailer->Host = PHPMAILERHOST;
            $mailer->Port = PHPMAILERPORT;
        }
        return $mailer->send();
    }

    function messageQueueFinished()
    {
        if (count($this->relays)) {
            foreach ($this->relays as $key => $val) {
                cl_output($key ."\t\t => ".sprintf('%d',$val['total']).' sent');
            }
        }
    }
}
